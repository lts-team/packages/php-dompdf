Source: php-dompdf
Section: php
Priority: optional
Maintainer: Debian PHP PEAR Maintainers <pkg-php-pear@lists.alioth.debian.org>
Uploaders: William Desportes <williamdes@wdes.fr>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-phpcomposer,
               fonts-dejavu,
               fonts-dejavu-core,
               fonts-dejavu-extra,
               fonts-liberation,
               php-cli,
               php-dompdf-svg-lib (>= 0.5.0),
               php-font-lib (>= 0.5.4),
               php-masterminds-html5,
               php-mockery,
               phpab,
               phpunit <!nocheck>,
               pkg-php-tools (>= 1.41~),
               sdop,
               texlive-binaries
Rules-Requires-Root: no
Standards-Version: 4.6.2
Homepage: http://dompdf.github.io
Vcs-Browser: https://salsa.debian.org/php-team/pear/php-dompdf
Vcs-Git: https://salsa.debian.org/php-team/pear/php-dompdf.git

Package: php-dompdf
Architecture: all
Multi-Arch: foreign
Depends: fonts-dejavu-core,
         fonts-dejavu-extra,
         sdop,
         ${misc:Depends},
         ${phpcomposer:Debian-require}
Suggests: ${phpcomposer:Debian-suggest}
Replaces: ${phpcomposer:Debian-replace}
Breaks: ${phpcomposer:Debian-conflict}, ${phpcomposer:Debian-replace}
Provides: ${phpcomposer:Debian-provide}
Description: HTML to PDF converter
 ${phpcomposer:description}. It:
  * handles most CSS 2.1 and a few CSS3 properties, including @import,
    @media & @page rules
  * supports most presentational HTML 4.0 attributes
  * supports external stylesheets, either local or through HTTP/FTP
    (via fopen-wrappers)
  * supports complex tables, including row and column spans, separate
    and collapsed border models, individual cell styling
  * supports images: GIF, PNG (8-, 24- and 32-bit with alpha channel),
    BMP, and JPEG
  * supports inline PHP
