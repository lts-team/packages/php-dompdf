#!/bin/sh

cat > lib/fonts/installed-fonts.dist.json << EOF 
{
    "sans-serif": {
        "normal": "Helvetica",
        "bold": "Helvetica-Bold",
        "italic": "Helvetica-Oblique",
        "bold_italic": "Helvetica-BoldOblique"
    },
    "times": {
        "normal": "Times-Roman",
        "bold": "Times-Bold",
        "italic": "Times-Italic",
        "bold_italic": "Times-BoldItalic"
    },
    "times-roman": {
        "normal": "Times-Roman",
        "bold": "Times-Bold",
        "italic": "Times-Italic",
        "bold_italic": "Times-BoldItalic"
    },
    "courier": {
        "normal": "Courier",
        "bold": "Courier-Bold",
        "italic": "Courier-Oblique",
        "bold_italic": "Courier-BoldOblique"
    },
    "helvetica": {
        "normal": "Helvetica",
        "bold": "Helvetica-Bold",
        "italic": "Helvetica-Oblique",
        "bold_italic": "Helvetica-BoldOblique"
    },
    "serif": {
        "normal": "Times-Roman",
        "bold": "Times-Bold",
        "italic": "Times-Italic",
        "bold_italic": "Times-BoldItalic"
    },
    "monospace": {
        "normal": "Courier",
        "bold": "Courier-Bold",
        "italic": "Courier-Oblique",
        "bold_italic": "Courier-BoldOblique"
    },
    "fixed": {
        "normal": "Courier",
        "bold": "Courier-Bold",
        "italic": "Courier-Oblique",
        "bold_italic": "Courier-BoldOblique"
    },
    "dejavu sans": {
        "bold": "DejaVuSans-Bold",
        "bold_italic": "DejaVuSans-BoldOblique",
        "italic": "DejaVuSans-Oblique",
        "normal": "DejaVuSans"
    },
    "dejavu sans mono": {
        "bold": "DejaVuSansMono-Bold",
        "bold_italic": "DejaVuSansMono-BoldOblique",
        "italic": "DejaVuSansMono-Oblique",
        "normal": "DejaVuSansMono"
    },
    "dejavu serif": {
        "bold": "DejaVuSerif-Bold",
        "bold_italic": "DejaVuSerif-BoldItalic",
        "italic": "DejaVuSerif-Italic",
        "normal": "DejaVuSerif"
    }
}
EOF
