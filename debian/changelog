php-dompdf (2.0.3+dfsg-2) unstable; urgency=medium

  * Move to fonts-liberation (Ref: #1038940) (Closes: #1039773)
  * Add a patch for phpunit 10 (Closes: #1039773)

 -- William Desportes <williamdes@wdes.fr>  Thu, 06 Jul 2023 11:20:10 +0200

php-dompdf (2.0.3+dfsg-1) unstable; urgency=medium

  * New upstream version 2.0.3 (CVE-2023-24813)

  [ William Desportes ]
  * Remove old config file from debian/
  * Update d/upstream/changelog

 -- William Desportes <williamdes@wdes.fr>  Wed, 08 Feb 2023 13:41:16 +0100

php-dompdf (2.0.2+dfsg-2) unstable; urgency=medium

  * Add missing closes to d/changelog (Closes: #978994)
  * Fix autopkgtest require
  * Add a Cpdf test
  * Fix wrong path for Cpdf in autoload
  * Fixup ImageTest because it uses a local test file
  * Add a DEP-8 test for basic-html-pdf.php example

 -- William Desportes <williamdes@wdes.fr>  Sat, 04 Feb 2023 15:17:50 +0100

php-dompdf (2.0.2+dfsg-1) unstable; urgency=medium

  [ Katharina Drexel ]
  * Refresh the packaging

  [ William Desportes ]
  * Remove dfsg fonts lib/fonts/*.afm
  * Remove dfsg fonts lib/fonts/DejaVu*.{ufm, ttf}
  * New upstream version 2.0.0
    - Closes: #1015874
    - Fixes CVE-2022-2400
    - Fixes CVE-2021-3902
    - Fixes CVE-2022-0085
    - Fixes CVE-2021-3838
  * New upstream version 2.0.1 (CVE-2022-41343)
  * New upstream version 2.0.2 (CVE-2023-23924)
  * Document copyrights of debian/* files
  * Set Uploaders to myself (Closes: #978994)
  * Remove Suggests: php-apcu, php-cli and php-tcpdf
  * Install the VERSION file because the code needs it
  * Add var/cache/php-dompdf/tmp to d/dirs
  * Add pkg-php-tools overrides and autoloaders
  * Add a patch to change directories to Debian folders
  * Add a patch for fontDir to work on build tests
  * Depend on fonts-dejavu-{extra,core} and sdop for fonts

 -- William Desportes <williamdes@wdes.fr>  Fri, 03 Feb 2023 22:53:19 +0100

php-dompdf (0.6.2+dfsg-3) unstable; urgency=medium

  * [8b6277c] Fix font loading during build (Closes: #820691)
  * [8b84346] Update dependencies for PHP 7 (Closes: #821571, #821696)

 -- Markus Frosch <lazyfrosch@debian.org>  Tue, 19 Apr 2016 21:14:52 +0200

php-dompdf (0.6.2+dfsg-2) unstable; urgency=medium

  * [a922437] Update php-font-lib dependency to ~0.2

 -- Markus Frosch <lazyfrosch@debian.org>  Mon, 29 Feb 2016 14:38:08 +0100

php-dompdf (0.6.2+dfsg-1) unstable; urgency=medium

  [ David Prévot ]
  * [dd91f93] Revert "Add ownCloud for Debian to uploaders"
  * [315a84c] Drop now useless minimal version for php-font-lib
  * [a470548] Fix watch file

  [ Markus Frosch ]
  * [9bd8627] Update watch to exclude beta releases and mark them dfsg
  * [d1a2d1a] Adopt package within the PHP team (Closes: #748604)
  * [9f53a9a] Imported Upstream version 0.6.2+dfsg, which fixes:
    * CVE-2014-5013
    * CVE-2014-5012
    * CVE-2014-5011
    * CVE-2014-2383
    (Closes: #813849)
  * [a4b6496] Update copyright to not mention DFSG removed files
  * [9fdc430] Bump standards version to 3.9.7

 -- Markus Frosch <lazyfrosch@debian.org>  Sun, 28 Feb 2016 18:09:26 +0100

php-dompdf (0.6.1+dfsg-2) unstable; urgency=medium

  * Document security issue fixed in last upstream version, and upload to
    unstable.

 -- David Prévot <taffit@debian.org>  Wed, 23 Apr 2014 15:23:50 -0400

php-dompdf (0.6.1+dfsg-1) experimental; urgency=medium

  * New upstream release, uploaded to experimental because of the php-font-lib
    dependency:
    - Fixes an arbitrary file read vulnerability, that requires
      DOMPDF_ENABLE_REMOTE (disabled by default) to be enabled.
      (Closes: #745619) [CVE-2014-2383]
  * Add ownCloud for Debian to uploaders
  * Bump standards version to 3.9.5
  * Use Files-Excluded feature of uscan
  * Use phpcomposer from pkg-php-tools
  * Ship upstream README.md
  * Update dependencies, as recommended upstream
  * Update copyright
  * Acknowledge upstream separation of cache and fonts
  * Update upstream changelog
  * Use internal CSS in example

 -- David Prévot <taffit@debian.org>  Sat, 12 Apr 2014 14:32:12 -0400

php-dompdf (0.6.0~beta3+dfsg0-1) unstable; urgency=low

  * Initial release (Closes: #567928)

 -- David Prévot <taffit@debian.org>  Sat, 23 Nov 2013 10:00:18 -0400
